import click
from flask import Flask, render_template
from hello_api.models import AutomotiveCertificate, database
from hello_api.api import AutomotiveCertificateAPI


DATABASE = "automotive_certificate.sqlite3"


app = Flask(__name__)
app.config.from_object(__name__)


@app.before_first_request
def before_first_request():
    database.init(app.config['DATABASE'])


@app.context_processor
def inject_repo():
    return dict(REPO='https://sources.delahayeyourself.info/sdelahaye/HelloAPI',
                REPOGIT='https://sources.delahayeyourself.info/sdelahaye/HelloAPI.git')

@app.route('/')
def index():
    return render_template('index.html')


AutomotiveCertificateAPI.register(app)


@app.cli.command()
@click.option('--populate-fake-data', default=False,
              help='Populate fake data when creatin database',
              type=click.BOOL)
def initdb(populate_fake_data):
    from hello_api.models import create_tables, generate_fake_data
    create_tables()
    if populate_fake_data:
        generate_fake_data()


@app.cli.command()
def dropdb():
    from hello_api.models import drop_tables
    drop_tables()
