from marshmallow import Schema, fields


class OwnerSchema(Schema):
    name = fields.Str()
    first_name = fields.Str()
    city = fields.Str()
    postal_code = fields.Str()
    address = fields.Str()


class AutomotiveCertificateSchema(Schema):
    license_plate = fields.Str()
    vin = fields.Str()
    release_date = fields.Date()
    manufacturer = fields.Str()
    owner = fields.Nested(OwnerSchema)
    uri = fields.Str()
