from peewee import Model, CharField, DateField, ForeignKeyField, SqliteDatabase
from flask import url_for


database = SqliteDatabase(None)


class BaseModel(Model):

    class Meta:
        database = database


class ReachableModel(Model):
    endpoint = None

    @property
    def uri(self):
        if type(self).endpoint:
            return url_for(type(self).endpoint,
                           pk=self.id,
                           _external=True)
        return None


class Owner(BaseModel):
    name = CharField(max_length=50)
    firstname = CharField(max_length=50)
    city = CharField(max_length=30)
    postal_code = CharField(max_length=5)
    address = CharField(max_length=150)


class AutomotiveCertificate(BaseModel, ReachableModel):

    endpoint = 'automotives_certificates_api'

    license_plate = CharField(max_length=13)
    vin = CharField(max_length=13)
    release_date = DateField()
    manufacturer = CharField(max_length=50)
    owner = ForeignKeyField(Owner, backref='vehicles')


def create_tables():
    with database:
        database.create_tables([Owner, AutomotiveCertificate, ])


def drop_tables():
    with database:
        database.drop_tables([Owner, AutomotiveCertificate, ])


def generate_fake_data():
    with database:
        from faker import Faker
        fake = Faker('fr_FR')
        for pk in range(0, 10):
            owner = generate_fake_owner(fake)
            generate_fake_vehicle_by_owner(fake, owner)


def generate_fake_owner(fake):
    return Owner.create(
        name=fake.last_name(),
        firstname=fake.first_name(),
        city=fake.city(),
        postal_code=fake.postcode(),
        address=fake.street_address()
    )


def generate_fake_vehicle_by_owner(fake, owner):
    import random
    for pk in range(0, random.randint(1, 3)):
        AutomotiveCertificate.create(
            owner=owner,
            license_plate=fake.license_plate(),
            vin=fake.ean13(),
            release_date=fake.date_between(),
            manufacturer=fake.company()
        )
