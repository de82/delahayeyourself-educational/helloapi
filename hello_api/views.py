from flask.views import MethodView
from flask import abort, jsonify, request
from peewee import ModelSelect

class ModelAPIMixin(MethodView):
    model = None
    schema = None
    schemas = None
    endpoint = None
    url = None
    view_func_endpoint = None

    def get_model(self):
        return type(self).model

    def get(self, **kwargs):
        raise NotImplementedError()

    def dispatch_request(self, *args, **kwargs):
        raw_result = super().dispatch_request(
            *args, **kwargs)
        return self.to_json(raw_result)

    def to_json(self, raw_result):
        return jsonify(self.to_dict(raw_result))

    def to_dict(self, raw_result):
        if isinstance(raw_result, ModelSelect):
            return type(self).schemas.dump(raw_result).data
        else:
            return type(self).schema.dump(raw_result).data

    @classmethod
    def view_func(cls):
        if not cls.view_func_endpoint:
            cls.view_func_endpoint = cls.as_view(cls.endpoint)
        return cls.view_func_endpoint

    @classmethod
    def register(cls, app):
        pass


class DetailsModelAPIMixin(ModelAPIMixin):
    pk = 'pk'
    pk_type = 'int'

    def execute_single_queryset(self, pk):
        instance = self.get_model().get(pk)
        if not instance:
            abort(404)
        return instance

    def get(self, **kwargs):
        if 'pk' in kwargs:
            return self.execute_single_queryset(kwargs.get('pk'))
        return super().get()

    @classmethod
    def register(cls, app):
        app.add_url_rule(
            '%s<%s:%s>' % (cls.url, cls.pk_type, cls.pk),
            view_func=cls.view_func(), methods=['GET', ]
        )
        super().register(app)

class ListModelAPIMixin(ModelAPIMixin):

    def execute_queryset(self):
        return self.get_model().select()

    def get(self, **kwargs):
        if not kwargs:
            return self.execute_queryset()
        return super().get(**kwargs)

    @classmethod
    def register(cls, app):
        app.add_url_rule(
            '%s' % cls.url,
            view_func=cls.view_func(), methods=['GET', ]
        )
        super().register(app)
