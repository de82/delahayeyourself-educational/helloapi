from hello_api.models import AutomotiveCertificate
from hello_api.views import ModelAPIMixin, DetailsModelAPIMixin, ListModelAPIMixin
from hello_api.schemas import AutomotiveCertificateSchema


class AutomotiveCertificateAPI(ListModelAPIMixin, DetailsModelAPIMixin,
                               ModelAPIMixin):

    model = AutomotiveCertificate
    endpoint = 'automotives_certificates_api'
    url = '/api/automotives/certificates/'
    schema = AutomotiveCertificateSchema()
    schemas = AutomotiveCertificateSchema(many=True)
