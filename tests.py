import os
import tempfile
import pytest

from app import app
from hello_api.models import (
    AutomotiveCertificate,
    create_tables,
    database,
    generate_fake_data
)
from flask import json


@pytest.fixture
def client():
    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    database.init(app.config['DATABASE'])
    client = app.test_client()

    with app.app_context():
        create_tables()
        generate_fake_data()

    yield client
    os.unlink(app.config['DATABASE'])
    os.close(db_fd)


def test_index_page(client):
    """Make sure index page is working"""
    rv = client.get('/')
    assert 200 == rv.status_code
    assert b'HelloAPI' in rv.data


def test_automotives_certificates_api(client):
    """Make sure /api/automotives/certificates/ is working"""
    rv = client.get('/api/automotives/certificates/')
    certificates = AutomotiveCertificate.select()
    data = json.loads(rv.get_data(as_text=True))
    assert 200 == rv.status_code
    assert rv.content_type == 'application/json'
    assert len(certificates) == len(data)
    for certificate in certificates:
        assert any(d['license_plate'] ==
                   certificate.license_plate for d in data)


def test_automotives_certificates_single_api(client):
    """Make sure /api/automotives/certificates/1 is working"""
    rv = client.get('/api/automotives/certificates/1')
    certificates = AutomotiveCertificate.get(1)
    data = json.loads(rv.get_data(as_text=True))
    assert 200 == rv.status_code
    assert rv.content_type == 'application/json'
    assert certificates.license_plate == data.get('license_plate')
    assert isinstance(data.get('owner'), dict)