# ![HelloAPI](static/logo.png)

> A simple and dummy REST API written in Python using Flask, Peewee and Marshmallow. 

## Quickstart

### Grab the code

```bash
$ git clone https://sources.delahayeyourself.info/sdelahaye/HelloAPI.git
```

#### Setup dev env

```bash
$ pipenv --python 3
$ pipenv install
```

#### Init database and start server

```bash
$ flask initdb --populate-fake-data true
$ flask run
```

#### Perform your first request with curl

```bash
$ curl http://127.0.0.1:5000/api/automotives/certificates/
```

### UML Diagramm

#### Database

![Uml diagram database](static/uml_hello_api.png)

#### Class

![Uml diagram](static/uml_hello_api_class.png)

### Requirements

```json
flask
peewee
faker
marshmallow
```

### Testings ?

Coverage is minimal but enough for learning some test unit in python with pytest

```bash
pytest tests.py
```

### Purpose

This project is only for educational purpose. It's actually a basic application for generating a dummy REST API.

### Authors

> [Samy Delahaye](https://delahayeyourself.info)
